import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Enumeration;

/**
 * Classe de utilitário para o KeyStore.
 */
public class KeyStoreUtils {

    private KeyStore keyStore;

    private String keyStoreName;
    private String keyStoreType;
    private String keyStorePassword;

    public KeyStoreUtils(String keyStoreType, String keyStorePassword, String keyStoreName) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        this.keyStoreName = keyStoreName;
        this.keyStoreType = keyStoreType;
        this.keyStorePassword = keyStorePassword;

    }

    /**
     * Cria uma KeyStore vazia.
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    void createEmptyKeyStore() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        if(keyStoreType ==null || keyStoreType.isEmpty()){
            keyStoreType = KeyStore.getDefaultType();
        }
        keyStore = KeyStore.getInstance(keyStoreType);
        char[] pwdArray = keyStorePassword.toCharArray();
        keyStore.load(null, pwdArray);

        // Salva o KeyStore
        FileOutputStream fos = new FileOutputStream(keyStoreName);
        keyStore.store(fos, pwdArray);
        fos.close();
    }

    /**
     * Cria uma nova entrada no KeyStore.
     * @param alias Nome pelo qual é chamada essa entrada.
     * @param secretKeyEntry Entrada no KeyStore (Chave)
     * @param protectionParameter Senha do KeyStore para inserção de nova entrada.
     * @throws KeyStoreException
     */
    void setEntry(String alias, KeyStore.SecretKeyEntry secretKeyEntry, KeyStore.ProtectionParameter protectionParameter) throws KeyStoreException {
        keyStore.setEntry(alias, secretKeyEntry, protectionParameter);
    }

    /**
     * Busca uma entrada no KeyStore.
     * @param alias Alias da entrada no KeyStore.
     * @return Entrada no KeyStore.
     * @throws UnrecoverableEntryException Exceção a ser lançada caso não seja possível recuperar a entrada.
     * @throws NoSuchAlgorithmException Exceção a ser lançada caso não reconheça o algoritmo.
     * @throws KeyStoreException Exceção a ser lançada caso haja erro no KeyStore.
     */
    KeyStore.Entry getEntry(String alias) throws UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException {
        KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(keyStorePassword.toCharArray());
        return keyStore.getEntry(alias, protParam);
    }

    KeyStore getKeyStore() {
        return this.keyStore;
    }
}
