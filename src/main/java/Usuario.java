import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.KeyStore;
import java.util.Scanner;

/**
 * Essa classe implementa Usuário no sistema.
 */
public class Usuario {

    private String nome;

    private String alias;

    private String chaveMestraKeyStore;

    private KeyStoreUtils keyStore;

    private IvParameterSpec ivParameterSpec;

    private Scanner scanner;

    /**
     * Construtor padrão da classe.
     * @param nome Nome do Usuário.
     * @param chaveMestraKeyStore Senha mestre do KeyStore, necessário para armazenar uma chave.
     * @param keyStore KeyStore compartilhado entre os Usuários.
     * @param iv Nounce.
     * @param scanner Scanner para capturar os inputs via terminal.
     */
    public Usuario(String nome, String chaveMestraKeyStore, KeyStoreUtils keyStore, String iv, Scanner scanner) {
        this.nome = nome;
        this.keyStore = keyStore;
        this.chaveMestraKeyStore = chaveMestraKeyStore;
        this.ivParameterSpec = new IvParameterSpec(iv.getBytes());
        this.alias = "test-alias";
        this.scanner = scanner;
    }

    /**
     * Envia uma mensagem a um Usuário.
     * @param message Mensagem a ser enviada.
     * @param receiver Usuário alvo da mensagem.
     * @throws Exception Exceção a ser enviada caso haja problemas na operação de cifragem.
     */
    public void sendMessage(String message, Usuario receiver) throws Exception {

        //Busca uma entrada com o alias designado.
        KeyStore.Entry entry = keyStore.getEntry(alias);

        //Caso não haja entrada, é criada a entrada.
        if (entry == null) {
            createKey(alias, ChavesUtils.gerarChaveAleatoria().getBytes());
            entry = keyStore.getEntry(alias);
        }

        //Da entrada retira-se a chave.
        Key key = ((KeyStore.SecretKeyEntry) entry).getSecretKey();

        //A mensagem é convertida em array de bytes.
        byte[] messageBytes = message.getBytes();

        //A classe Cipher é utilizada para criptografar a mensagem com o algoritmo AES utilizando o BouncyCastle como Provider.
        Cipher encryptCipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");

        //Passa os parâmetros chave e IV para realizar o processo.
        encryptCipher.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec);

        System.out.println(nome + " está enviando uma mensagem para " + receiver.nome + ".");

        //Ciptografa a mensagem e a "envia"
        receiver.receiveMessage(encryptCipher.doFinal(messageBytes), this);
    }

    /**
     * Recebe uma mensagem de um Usuário.
     * @param message Mensagem recebida.
     * @param usuario Usuário que envivou a mensagem.
     * @throws Exception Exceção a ser enviada caso haja problemas na operação de decifragem.
     */
    public void receiveMessage(byte[] message, Usuario usuario) throws Exception {
        System.out.println("Mensagem cifrada: " + new String(message, "UTF-8"));

        //Busca uma entrada com o alias designado.
        KeyStore.Entry entry = keyStore.getEntry(alias);

        //Caso não haja entrada, o programa sai pois não houve envio.
        if (entry == null) {
            System.out.println("Não ha nenhum registro na KeyStore.");
            System.out.println("Encerrando processo...");
            return;
        }

        //Da entrada retira-se a chave.
        Key key = ((KeyStore.SecretKeyEntry) entry).getSecretKey();

        //A classe Cipher é utilizada para decifrar a mensagem com o algoritmo AES utilizando o BouncyCastle como Provider.
        Cipher decryptCipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");

        //Passa os parâmetros chave e IV para realizar o processo.
        decryptCipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);

        System.out.println("Você recebeu uma mensagem de " + usuario.nome
                + ": " + new String(decryptCipher.doFinal(message), "UTF-8"));
        System.out.println("Deseja responder? (S/N)");
        if (scanner.nextLine().toUpperCase().equals("S")) {
            System.out.println("Digite sua mensagem para " + usuario.nome + ": ");
            sendMessage(scanner.nextLine(), usuario);
        } else {
            return;
        }
    }

    /**
     * Cria a chave para cifragem/decifragem de mensagens.
     * @param alias Alias da chave no KeyStore.
     * @param key Chave em texto plano.
     * @throws Exception Mensagem a ser enviada caso haja problema na operação.
     *
     */
    public void createKey(String alias, byte[] key) throws Exception {
        //Seleciona o algoritmo que utilizará para criptografar a chave.
        SecretKey secretKey = new SecretKeySpec(key, "AES");
        KeyStore.SecretKeyEntry keyEntry = new KeyStore.SecretKeyEntry(secretKey);
        KeyStore.ProtectionParameter protectionParameter = new KeyStore.PasswordProtection(chaveMestraKeyStore.toCharArray());
        keyStore.setEntry(alias, keyEntry, protectionParameter);
    }





}
