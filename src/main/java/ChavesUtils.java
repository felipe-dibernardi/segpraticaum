import java.security.SecureRandom;

/**
 * Classe utilitária para geração de chave.
 */
public class ChavesUtils {

    /**
     * Gera uma chave aleatória de 128 bits.
     * @return Chave aleatória.
     */
    public static String gerarChaveAleatoria(){
        char[] chars = "QWERTYUIOPASDFGHJKLZXCVBNMabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*".toCharArray();
        StringBuilder sb = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 16; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    /**
     * Gera um IV aleatório de 128 bits.
     * @return IV aleatório.
     */
    public static String gerarIV() {
        char[] chars = "123456789abcdef".toCharArray();
        StringBuilder sb = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 16; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
