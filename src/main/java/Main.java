import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws Exception {

        //Adição de provider da BouncyCastle.
        Security.addProvider(new BouncyCastleProvider());

        //Criação da chave mestre do KeyStore
        String chaveMasterKeyStore = ChavesUtils.gerarChaveAleatoria();

        //Criação de um IV
        String iv = ChavesUtils.gerarIV();

        //Inicialização do Utils do KeyStore
        KeyStoreUtils keyStore = new KeyStoreUtils("JCEKS", chaveMasterKeyStore, "B&AKS");

        //Criação do KeyStore
        keyStore.createEmptyKeyStore();

        //Processo de troca de mensagens
        Usuario bob = new Usuario("Bob", chaveMasterKeyStore, keyStore, iv, scanner);
        Usuario alice = new Usuario("Alice", chaveMasterKeyStore, keyStore, iv, scanner);

        System.out.println("Seja bem-vindo Bob. Digite sua mensagem para Alice: ");
        String message = scanner.nextLine();

        bob.sendMessage(message, alice);

    }
}
